library(tidyverse)

nums <- 10:99
data <- expand.grid(nums, nums)
data |>
  mutate(value1 = Var1/Var2,
         digit1 = floor(Var1/10),
         digit1_2 = Var1 %% 10,
         digit2 = floor(Var2/10),
         digit2_2 = Var2 %% 10) |>
  filter(value1 < 1) |>
  mutate(test = pmap(list(digit1, digit1_2, digit2, digit2_2),
                     function(a, b, c, d) {
                       any(c(a, b) %in% c(c, d))
                     })) |>
  filter(test == T,
         (digit1_2 != 0 & digit2_2 != 0)) |>
  mutate(value2 = pmap_dbl(list(digit1, digit1_2, digit2, digit2_2),
                     function(a, b, c, d) {
                       n <- unique(c(a, b))
                       m <- unique(c(c, d))
                       x <- c(a, b) %in% c(c, d)
                       if(length(n) == length(m)) {
                         # length of n and m are the same
                         if(length(n) == 1) {
                           # n == 1 and m == 1
                           return(a/c)
                         } else {
                           # n == 2 and m == 2
                           if(all(x)) {
                             # ex. 12 and 21
                             return(1)
                           } else {
                             if(any(x)) {
                               # ex. 12 and 23
                               return(n[!n %in% m]/m[!m %in% n])
                             } else {
                               # ex. 12 and 34
                               return(1)
                             }
                           }
                         }
                       } else {
                         # length of n and m are different
                         if(length(n) == 1) {
                           # n == 1 and m == 2
                           return(n/(m[m != n]))
                         } else {
                           # n == 2 and m == 1
                           return((n[n != m])/m)
                         }
                       }
                       return(NA)
                     })) |>
  filter(value2 < 1) |>
  filter(value1 == value2) |>
  summarise(ans = fractions(prod(value2)) |> str_split("/") |> pluck(1, 2) |> as.double())


         